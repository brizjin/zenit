from oracle_client.oracle_client import OracleClient

from ze.dvn_msg import DvnMsg


class Ze(OracleClient):
    def __init__(self, oracle_connection_string):
        super().__init__(oracle_connection_string)

        self.dvn_msg = DvnMsg(self, 'DVN_MSG')
