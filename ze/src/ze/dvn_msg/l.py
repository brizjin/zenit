import cx_Oracle

from client.base import AbsMethod


class L(AbsMethod):
    def request_interface(self, xml):
        ret = self.abs.execute_plsql("""
            begin
                :xml_out := Z$DVN_MSG_L.request_interface(:xml);
            end;
        """, xml=xml, xml_out=cx_Oracle.CLOB)
        return ret.get('xml_out')

    def request(self, xml):
        ret = self.abs.execute_plsql("""
            begin
                :xml_out := Z$DVN_MSG_L.request(:xml);
            end;
        """, xml=xml, xml_out=cx_Oracle.CLOB)
        return ret.get('xml_out')
