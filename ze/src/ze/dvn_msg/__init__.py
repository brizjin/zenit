from client.base import AbsClass
from ze.dvn_msg.ext import Ext
from ze.dvn_msg.l import L


class DvnMsg(AbsClass):
    def __init__(self, connection, abs_class_name):
        super().__init__(connection, abs_class_name)

        self.ext = Ext(self.abs)
        self.l = L(self.abs)
