import cx_Oracle

from client.base import AbsMethod


class Ext(AbsMethod):
    def request(self, xml):
        ret = self.abs.execute_plsql("""
            begin
                :xml_out := Z$DVN_MSG_EXT.request(:xml);
            end;
        """, xml=xml, xml_out=cx_Oracle.CLOB)
        return ret.get('xml_out')
