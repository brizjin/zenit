import uuid
from datetime import datetime

import xmltodict
from hamcrest import *

from helpers.string import lstripn_dedent
from helpers.xml import make_xml


def test_ping(cnn):
    response = cnn.dvn_msg.l.request_interface("<PING/>")
    assert_that(response, equal_to('<PING>PONG</PING>\n'))


def test_gen_ping(cnn):
    response = cnn.dvn_msg.l.request_interface(make_xml('PING', **dict()))
    assert_that(response, equal_to('<PING>PONG</PING>\n'))


def test_gen_ext_ping(cnn):
    xml = make_xml(
        'REQUEST',
        METHOD='PING',
        PARAMS='',
        CONTEXT=lstripn_dedent("""
            <HTTP_HEADERS>
                <COOKIE>SESSIONID=123</COOKIE>
            </HTTP_HEADERS>"""),
        MESSAGE_ID=uuid.uuid4(),
        CORRELATION_ID=uuid.uuid4(),
        TS=datetime.now(),
        TTL=10)
    response = cnn.dvn_msg.ext.request(xml)
    assert_that(xmltodict.parse(response)['RESPONSE']['RESULT'], equal_to('PONG'))
