import os

import xmltodict
from hamcrest import *
from pandas import DataFrame

import config
from helpers.log import get_logger
from helpers.string import lstripn_dedent

logger = get_logger(__file__)


def test_get_banks(cnn):
    """Получим все изменения по банкам на дату"""
    text = cnn.dvn_msg.l.request_interface(lstripn_dedent("""
        <GET_BANKS>
            <DATE_BEGIN>01.10.2018</DATE_BEGIN>
        </GET_BANKS>
    """))
    df = DataFrame(xmltodict.parse(text)['GET_BANKS']['BANKS']['BANK'])
    logger.debug(f"\n{df}")
    # with open(os.path.join(config.PROJECT_ROOT_DIR, 'out', 'get_banks.txt'), 'w') as f:
    #     f.write(text)
    assert_that(df.index, has_length(21569))


def test_get_banks_active(cnn):
    """Получим все активные банки"""
    text = cnn.dvn_msg.l.request_interface(lstripn_dedent("""<GET_BANKS></GET_BANKS>"""))
    df = DataFrame(xmltodict.parse(text)['GET_BANKS']['BANKS']['BANK'])
    # logger.debug(f"\n{df}")
    # with open(os.path.join(config.PROJECT_ROOT_DIR, 'out', 'get_banks.txt'), 'w') as f:
    #     f.write(text)
    assert_that(df.index, has_length(1877))
