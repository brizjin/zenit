from hamcrest import *

from helpers.tests import AbsTestCase


class TestDual(AbsTestCase):
    def test_dual(self):
        df = self.abs.select("select * from dual")
        assert_that(df.index, has_length(1))

    def test_verision(self):
        df = self.abs.select("SELECT * FROM V$VERSION")
        assert_that(df.index, has_length(5))

    def test_dbms_aq_exisrs(self):
        df = self.abs.select("""
            select OBJECT_NAME, OWNER
            from SYS.ALL_OBJECTS
            where UPPER(OBJECT_TYPE) = 'PACKAGE'
              and OBJECT_NAME like '%DBMS%'
              and OBJECT_NAME = 'DBMS_AQ'
            order by OWNER, OBJECT_NAME """)
        assert_that(df.index, has_length(1))

    def test_log_aq_objects(self):
        log_table = 'DVN_MSG_LOG_TABLE'
        log_type = 'DVN_MSG_LOG_AQ_TYPE'
        log_type = 'DVN_MSG_LOG_AQ_TYPE'
        log_aq = 'DVN_MSG_LOG'
        df = self.abs.select(f"select * from ALL_TYPES where type_name = '{log_type}'")
        assert_that(df.index, has_length(1))
        df = self.abs.select(f"select * from ALL_QUEUES where name = '{log_aq}'")
        assert_that(df.index, has_length(1))
        assert_that(df.iloc[0].QUEUE_TABLE, log_table)
        df = self.abs.select(f"select * from {log_table}")
        assert_that(df.index, has_length(0))
