from pytest import fixture

import config
from helpers.log import get_logger
from ze.instance import Ze

logger = get_logger(__file__)


def pytest_make_parametrize_id(config, val, argname):
    return f"{argname}: {repr(val)}"


@fixture(scope='session')
def cnn():
    abs_instance = Ze(config.DEFAULT_CONNECTION_STRING)
    yield abs_instance
    logger.debug("rollback")
    # abs_instance.connection.commit()
    abs_instance.connection.rollback()
