import xmltodict
from hamcrest import *

from helpers.string import lstripn_dedent




def test_ping(cnn):
    response = cnn.dvn_msg.l.request_interface("<PING/>")
    assert_that(response, equal_to('<PING>PONG</PING>\n'))


def test_ping2(cnn):
    """В валидном запросе в ответе должен быть RESULT"""
    response = cnn.dvn_msg.ext.request(lstripn_dedent("""
        <REQUEST>
          <METHOD>ping</METHOD>
          <PARAMS/>
          <CONTEXT>
            <HTTP_HEADERS>
              <COOKIE>SESSIONID=123</COOKIE>
            </HTTP_HEADERS>
          </CONTEXT>
          <MESSAGE_ID>345-12341-123412-234</MESSAGE_ID>
          <CORRELATION_ID>1234-1234-1234-1234</CORRELATION_ID>
          <TS>1500000000</TS>
          <TTL>15</TTL>
        </REQUEST>
    """))
    assert_that(xmltodict.parse(response)['RESPONSE']['RESULT'], equal_to('PONG'))


def test_invalid_xml_request(cnn):
    """Ошибка если инвалидный xml"""
    response = cnn.dvn_msg.ext.request(lstripn_dedent("""
        <REQUEST>
          <METHOD1>ping</METHOD>
          <PARAMS/>
          <CONTEXT>
            <HTTP_HEADERS>
              <COOKIE>SESSIONID=123</COOKIE>
            </HTTP_HEADERS>
          </CONTEXT>
          <MESSAGE_ID>345-12341-123412-234</MESSAGE_ID>
          <CORRELATION_ID>1234-1234-1234-1234</CORRELATION_ID>
          <TS>1500000000</TS>
          <TTL>15</TTL>
        </REQUEST>
    """))
    error_key = xmltodict.parse(response)['RESPONSE']['ERROR']['KEY']
    expected_error_text_in_key = 'тег конца элемента "METHOD" не соответствует тегу начала элемента "METHOD1"'
    assert_that(error_key, contains_string(expected_error_text_in_key))
