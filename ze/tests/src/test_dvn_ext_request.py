from hamcrest import *

import config
from helpers.log import get_logger
from ze.instance import Ze

logger = get_logger(__file__)


def test_connect():
    cnn = Ze(config.DEFAULT_CONNECTION_STRING)
    df = cnn.select("select * from dual")
    assert_that(df.index, has_length(1))


registry = {}


def register_class(target_class):
    registry[target_class.__name__] = target_class


class Meta(type):
    def __new__(meta, name, bases, class_dict):
        cls = type.__new__(meta, name, bases, class_dict)
        register_class(cls)
        return cls


class ConnectedObject(metaclass=Meta):
    def __init__(self, connection):
        self.connection = connection


class Cls1(ConnectedObject):
    def dual(self):
        return self.connection.select("select * from dual")


class Cls2(ConnectedObject):
    pass


class Cls3(Cls2, Cls1):
    pass


class Cls4(Cls3):
    pass


class Manager:
    def __init__(self, connection):
        for key, value in registry.items():
            self.__dict__[key.lower()] = value(connection)


def test_load_ext():
    # print(registry)
    m = Manager(Ze(config.DEFAULT_CONNECTION_STRING))
    df = m.cls1.dual()
    print(df)
# ze_instance = Ze(config.DEFAULT_CONNECTION_STRING)
# df = ze_instance.select("select * from dual")
# assert_that(df.index, has_length(1))
# ze_instance.dvn_msg.ext.request()
# clsmembers = inspect.getmembers(sys.modules['ze'], inspect.isclass)
# clsmembers = inspect.getmembers(sys.modules['ze'], inspect.isclass)
# pprint(clsmembers)
# module_members = inspect.getmembers(sys.modules['generators'], inspect.ismodule)
# module = importlib.import_module("ze")
#
# for member in dir(module):
#     handler_class = getattr(module, member)
#
#     if handler_class and inspect.isclass(handler_class):
#         print(member)
# package = ze
#
# for w in pkgutil.walk_packages(package.__path__):
#     logger.debug(w)
# for importer, modname, ispkg in pkgutil.iter_modules(package.__path__):
#     print("Found submodule %s (is a package: %s)" % (modname, ispkg))
