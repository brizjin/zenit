from helpers.tests import AbsTestCase


class TestCreate(AbsTestCase):
    def test_create_all_aq(self):
        # self.abs.execute_plsql("begin Z$DVN_MSG_CREATE_SCRIPTS.CREATE_ALL_AQ; end;")
        self.abs.execute_plsql("begin Z$DVN_MSG_CREATE_SCRIPTS.create_process_logs_job; end;")

    def test_drop_all_aq(self):
        self.abs.execute_plsql("begin Z$DVN_MSG_CREATE_SCRIPTS.drop_all_aq; end;")
