# python-abs-tests
При настройке проекта могут возникнуть проблемы с настройкой ораклового клиента:
 * Проще всего решается скачиванием Oracle Instant Client нужной разрадности.
 * Затем разархивируем в любой папке, например в папке с клиентами, например такой C:\app\BryzzhinIS\product\11.2.0\
 * Прописываем путь до клиента в системный PATH
 * В папке клиента создаем папку network\admin и туда подкладываем наш tnsnames.ora, файл можно найти в resources\tnsnames.ora
 * Теперь сокращеные tns из примера конфига должны работать

Все параметры тестов берутся из конфиг файла <b>config/config.yml</b>. Пароль к тестовым базам нужно будет прописать. Например:
```yaml
TNS:
  ass: ibs/123@lw-ass-abs
  abs: ibs/123@lw-abs-abs
  p2: ibs/123@lw-p2-abs
  msb: ibs/123@msb
  mid: ibs/123@midabs
  day: ibs/123@day
  midday: ibs/123@MIDEVERYDAY
  ssd: ibs/123@mid-abs-ssd
  p2@p2: portal2/123@lw-p2-abs
DEFAULT_TNS: ass

# папка куда можно складывать выгрузки из датафреймов
# если не указана то PROJECT_ROOT_DIR\csv
# CSV_FOLDER: C:\Users\BryzzhinIS\Documents\CSV

# настройки логгирования
LOGGING:
  version: 1
  disable_existing_loggers: False
  formatters:
    simple:
      format: "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    full:
      format: '%(asctime)s [%(name)s:%(lineno)s] [%(levelname)s] [%(processName)s,%(threadName)s] %(funcName)s - %(message)s'

  handlers:
    error_console:
      class: logging.StreamHandler
      level: DEBUG
      formatter: full
      stream: ext://sys.stderr # еще может быть ext://sys.stdout

    debug_file_handler:
      class: helpers.log.FileHandler
      level: DEBUG
      formatter: simple
      filename: logs/info.log
      encoding: utf8
      delay: True

    error_file_handler:
      class: helpers.log.RotatingFileHandler  # logging.handlers.RotatingFileHandler
      level: DEBUG
      formatter: full
      # если указать относительный путь, то путь считается от корня проекта
      filename: logs/errors.log
      maxBytes: 10485760 # 10MB
      backupCount: 20
      encoding: utf8
      delay: True

  loggers:
    oracle_client.oracle_client:
      level: DEBUG
      handlers: [error_console]
      propagate: no

  root:
    level: DEBUG
    handlers: [error_console, error_file_handler]

# Настройки ТестРейла
TESTRAIL:
  URL: https://testrail.dev.avanpos.com
  USER: i.bryzzhin@modulbank.ru
  PASS: Qq111111
  RUN_ID: 459
  USE: False

 ```
 
 Шаблон для создания тестов:
 ```python
from helpers.tests import AbsTestCase


class TestSomeClass(AbsTestCase):
    def test_my_test(self):
        self.assertEqual(1, 1)
```