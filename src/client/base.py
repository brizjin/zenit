# noinspection PyUnresolvedReferences
from cx_Oracle import NUMBER, STRING

from helpers.jinja import template_render


class AbsConnectedObject:
    abs = None

    def __init__(self, connection):
        self.abs = connection


class AbsClass(AbsConnectedObject):
    def __init__(self, connection, abs_class_name=None):
        super().__init__(connection)
        self.abs_class_name = abs_class_name or self.__class__.__name__

    def get(self, object_id, qual):
        """
        Возвращает текстовое значение реквизита объекта
        :param object_id: ID объекта в числовой форме или текстовое значение умолчательного реквизита
        :param qual: имя реквизита
        :return: текстовое значение реквизита, если имя реквизита было передано, иначе метод вернет ID

        Примеры:
            abs.main_docum.get(16629553871, 'NAZN') - получить назначение платежа документа
            abs.ft_money.get('EUR', 'VAL_COURCE') - получить текущий курс евро
            abs.ac_fin.get('40702810300000000480', 'CLIENT_V.INN') - получить ИНН клиента по счету
        """
        plsql = f"""
                begin
                    :value := IBS.Z#{self.abs_class_name}#INTERFACE.get$value(:object_id, :qual);
                end;
            """
        return self.abs.execute_plsql(plsql, object_id=object_id, qual=qual, value=STRING)['value']

    def set(self, object_id, qual, value):
        """
        Устанавливает значение реквизита объекта
        :param object_id: ID объекта
        :param qual: имя реквизита
        :param value: текстовое значение реквизита
        :return: None

        Примеры:
            abs.main_docum.set(16629553871, 'NAZN', 'Новое назначение платежа') - изменить назначение платежа документа
        """
        class_name = self.__class__.__name__
        plsql = f"""
            begin
                IBS.Z#{self.abs_class_name}#INTERFACE.set$value(:object_id, :qual, :value);
            end;
        """
        self.abs.execute_plsql(plsql, object_id=object_id, qual=qual, value=value)

    def execute_plplus_locate(self, condition_str):
        """
        Выполняет поиск объекта по условию (по умолчанию не строгий поиск).
        Если записей удовлетворяющих условиям нет, то возникает ошибка NO_DATA_FOUND.

        С передачей в condition_str любого условия сравнения, допустимого в модификаторе %locate
            :param condition_str: условие поиска, аналогично модификатору %locate
            :return: ID объекта

            Примеры:
                найти договор РКО по номеру счету:
                abs.rko.locate("x where x.[ACCOUNT].[MAIN_V_ID] = '40802810970010095779'")
                abs.rko.locate(f"x exact where x.[CLIENT]={client_id}
                                           and x.[RKO_TYPE]=::[RKO_TYPES]([CODE]='001_RKO_CARD')
                    and x.[DATE_CLOSE] is null") - найти карточный договор РКО клиента, строгий поиск

        """
        plplus = f"""
            declare
                obj_id number;
            begin
                obj_id := ::[{self.abs_class_name}]%locate({condition_str});
                :id := obj_id;
            end;
        """
        response = self.abs.execute_plsql(self.abs.class_utils.parse_plplus(plplus), id=NUMBER)
        return int(response['id'])

    def locate(self, **kwargs):
        if len(kwargs.keys()) != 1:
            raise Exception('Возможно передавать только один аттрибут')
        key, value = kwargs.popitem()
        return self.execute_plplus_locate(
            template_render("x where x.[{{key}}]={{value|sqlvar}}", key=key, value=value))


class AbsMethod(AbsConnectedObject):
    pass


class AbsView(AbsConnectedObject):
    pass


class AbsGen(AbsConnectedObject):
    pass


class AbsFunc(AbsConnectedObject):
    pass


class AbsManager(AbsConnectedObject):
    pass
