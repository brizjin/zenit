import pandas as pd

from helpers.log import get_logger

logger = get_logger(__file__)


class BaseSeriesFuctory:
    __slots__ = ["connection", "cls"]

    def __init__(self, connection, cls):
        self.connection = connection
        self.cls = cls

    def __call__(self, *args, **kwargs):
        return self.cls(self.connection, *args, **kwargs)


class BaseSeries(pd.Series):
    @property
    def _constructor(self):
        return BaseSeries._internal_ctor

    @property
    def _constructor_expanddim(self):
        return ConnectedFrame

    @classmethod
    def _internal_ctor(cls, *args, **kwargs):
        kwargs['connection'] = None
        return cls(*args, **kwargs)

    _metadata = ['connection']

    def __init__(self, connection, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connection = connection


class ConnectedFrame(pd.DataFrame):
    series_class = BaseSeries

    @property
    def _constructor(self):
        return ConnectedFrame._internal_ctor

    @property
    def _constructor_sliced(self):
        return BaseSeriesFuctory(self.connection, self.series_class)

    _metadata = ['connection']

    @classmethod
    def _internal_ctor(cls, *args, **kwargs):
        kwargs['connection'] = None
        return cls(*args, **kwargs)

    def __init__(self, connection, data, index=None, columns=None, dtype=None, copy=True):
        super().__init__(data=data, index=index, columns=columns, dtype=dtype, copy=copy)
        self.connection = connection

    def select_levels(self, n):
        return self.connection.select(f"select level from dual connect by level < {n}")


class BaseFrame(ConnectedFrame):
    def __init__(self, connected_data, index=None, columns=None, dtype=None, copy=True):
        super().__init__(connected_data.connection, connected_data, index=index, columns=columns, dtype=dtype,
                         copy=copy)
