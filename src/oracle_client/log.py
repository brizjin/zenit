import logging
import os
import time
from functools import reduce


def get_logger(logger_name):
    format_string = '%(asctime)s [{0}] [%(levelname)s] [%(processName)s,%(threadName)s] %(funcName)s - %(message)s'
    format_string = format_string.format(logger_name)

    logger = logging.getLogger(logger_name)
    if len(logger.handlers) == 0:
        # logger.setLevel(logging.DEBUG)

        file_handler = False
        if file_handler:
            logger_file_name = os.path.join('../logs/', logger_name + '.log')
            if not os.path.exists(os.path.dirname(logger_file_name)):
                os.makedirs(os.path.dirname(logger_file_name))
            file_handler = logging.FileHandler(logger_file_name)

        handlers = [logging.StreamHandler()] + ([file_handler] if file_handler else [])

        # formatter = logging.Formatter(format_string, datefmt='%Y-%m-%d %H:%M:%S.%f')
        formatter = logging.Formatter(format_string)
        for h in handlers:
            h.setFormatter(formatter)
            h.setLevel(logging.DEBUG)
            logger.addHandler(h)
    return logger


def seconds_to_str(t):
    return "%d:%02d:%02d.%03d" % reduce(lambda ll, b: divmod(ll[0], b) + ll[1:], [(t * 1000,), 1000, 60, 60])


logger = get_logger(__name__)


def log_call(func):
    def pr(s):
        # return "\n".join(map(lambda a: "│   " + a, str(s).split("\n")))
        return "\n".join(map(lambda a: "    " + a, str(s).split("\n")))

    def dict_str(d):
        if d:
            max_key_len = len(max(d.keys(), key=lambda a: len(a)))
            max_val_len = len(str(max(d.values(), key=lambda a: len(str(a)))))
            s = ""
            for i, (k, v) in enumerate(d.items()):
                s += str(k).ljust(max_key_len, " ") + " : " + str(v).ljust(max_val_len, " ") + "\n"
            return s.strip("\n")

    def decorator(*args, **kwargs):
        begin_time = time.time()
        logger.debug("┌── " + func.__module__ + "." + func.__qualname__)
        for i, arg in enumerate(args):
            logger.debug(pr(str(i).ljust(len(args) % 10, " ") + str(arg)))
        if kwargs:
            logger.debug(pr(dict_str(kwargs)))
        res = None
        try:
            res = func(*args, **kwargs)
            return res
        finally:
            pass
            if res is not None:
                logger.debug("├──────────")
                if type(res) == dict:
                    logger.debug(pr(dict_str(res)))
                else:
                    logger.debug(pr(str(res)))
            logger.debug("└──" + seconds_to_str(time.time() - begin_time))

    # if config.DEBUG_SELECT_RESULTS:
    return decorator
    # else:
    #    return func


def only_cx_oracle_vars(d1,d2):
    new_dict = dict()
    for key, value in d.items():
        if "cx_Oracle" in str(type(value)):
            new_dict[key] = value
    return new_dict