import xml

from lxml import etree
import xmltodict


class ABSProcedureResponse(object):
    def __init__(self, abs_raw_response):
        self.__abs_raw_response = abs_raw_response

    def get_raw_data(self):
        """
        Get raw response data as string

        :rtype: `str`
        """
        return self.__abs_raw_response

    def get_xml_representation(self):
        """
        Get ABS procedure response as element tree

        :rtype: `lxml.etree._Element`
        """
        try:
            return etree.fromstring(self.__abs_raw_response)
        except etree.XMLSyntaxError as parsing_error:
            raise RuntimeError(
                "Unable to parse ABS response to element tree\nABS response: {abs_response}\nError: {error}".format(
                    abs_response=self.__abs_raw_response, error=str(parsing_error)))

    def get_dictionary_representation(self):
        """
        Get ABS procedure response as python dictionary

        :rtype: `dict`
        """
        try:
            return xmltodict.parse(self.__abs_raw_response)
        except xml.parsers.expat.ExpatError as parsing_error:
            raise RuntimeError(
                "Unable to parse ABS response to dict\nABS response: {abs_response}\nError: {error}".format(
                    abs_response=self.__abs_raw_response, error=str(parsing_error)))
