"""
Oracle client for python projects

See installation guide ODPI-C Installation there:
    https://oracle.github.io/odpi/doc/installation.html
"""

import contextlib
import pprint
import textwrap
import time

import cx_Oracle
import numpy
import pandas as pd

from oracle_client.log import get_logger, seconds_to_str

# logger = get_logger(__name__.split('.')[0])
logger = get_logger(__name__)

ljust_number = 30


def pretty_print(caption, text):
    text = textwrap.indent(textwrap.dedent(str(text).lstrip('\n')), ' ' * 4).rstrip()
    if text.count('\n') > 0:
        caption += '\n'
    else:
        caption = caption.ljust(ljust_number)
        text = text.lstrip()
    return caption + text


class OracleClient(object):
    def __init__(self, oracle_connection_string, encoding="UTF-8"):
        """
        :param oracle_connection_string: database connection string.
            Example: "db_user_login/db_user_password@oracle.base.host:1521/db_name"
        :param encoding: sets the encoding to use for regular database strings
        """

        self.connection_string = oracle_connection_string

        # user_ = str(connection_string[:connection_string.find('/')])
        # pass_ = str(connection_string[connection_string.find('/') + 1:connection_string.find('@')])
        # dsn_ = str(connection_string[connection_string.find('@') + 1:])
        # cnn = cx_Oracle.Connection(user=user_, password=pass_, dsn=dsn_)

        # if "ORACLE_HOME" not in os.environ:
        #     raise Exception("Переменная окружения ORACLE_HOME не найдена")

        logger.debug("Подключаемся по строке соединения %s" % self.connection_string)
        self.connection = cx_Oracle.Connection(oracle_connection_string, encoding=encoding)
        logger.debug("Успешно соединились")
        # нужно чтобы LOB переменные на уровне клиента конвертились сразу в строку
        # это сильно повышает скорость работы с большим кол-вом маленьких LOBов
        self.connection.outputtypehandler = self.output_type_handler

    def __del__(self):
        try:
            self.connection.close()
        except AttributeError:
            logger.warning("Connection to database already closed")

    # def insert(self):
    #     raise NotImplementedError()
    #
    # def update(self):
    #     raise NotImplementedError()

    @staticmethod
    def output_type_handler(cursor, name, default_type, size, precision, scale):
        # types = {cx_Oracle.CLOB: cx_Oracle.LONG_STRING,
        #          cx_Oracle.BLOB: cx_Oracle.LONG_BINARY,
        #          cx_Oracle.LOB: cx_Oracle.LONG_STRING}
        if default_type == cx_Oracle.CLOB:
            return cursor.var(cx_Oracle.LONG_STRING, arraysize=cursor.arraysize)
        if default_type == cx_Oracle.BLOB:
            return cursor.var(cx_Oracle.LONG_BINARY, arraysize=cursor.arraysize)
        if default_type == cx_Oracle.LOB:
            return cursor.var(cx_Oracle.LONG_STRING, arraysize=cursor.arraysize)
        # return cursor.var(types[default_type], arraysize=cursor.arraysize)

    def execute_plsql(self, pl_sql_text, **kwargs):
        """
        Execute PL/SQL script

        :param pl_sql_text: PL/SQL text for execute
        :param kwargs: named parameters passed to the script
        :return: dict with parameter values

        Example:
            from oracle_client.oracle_client import OracleClient
            ora = OracleClient('username/password@tnsname')
            script = ''' begin
                             :c := :a + :b;
                         end; '''
            ret = ora.execute_plsql(script, a = 1, b = 5, c = ora.NUMBER)
            print(ret['c'])
        """
        begin_time = time.time()
        cursor_vars = {}
        with contextlib.closing(self.connection.cursor()) as cursor:
            if kwargs.__len__() > 0:
                logger.debug(pretty_print("запрос: ", pl_sql_text))
                logger.debug(pretty_print("параметры: ", pprint.pformat(kwargs, width=300)))
            else:
                logger.debug(pretty_print("запрос без параметров: ", pl_sql_text))

            out_vars = {}
            oracle_types = [cx_Oracle.CLOB, cx_Oracle.BLOB, cx_Oracle.NCHAR, cx_Oracle.STRING, cx_Oracle.NUMBER,
                            cx_Oracle.TIMESTAMP]
            for k, v in kwargs.items():

                if v in oracle_types:
                    cursor_vars[k] = cursor.var(v)
                # elif v == cx_Oracle.BOOLEAN:
                #     v = cursor.var(cx_Oracle.BOOLEAN)
                #     v.setvalue(0, True)
                #     cursor_vars[k] = v
                elif isinstance(v, numpy.generic):
                    cursor_vars[k] = numpy.asscalar(v)
                else:
                    # if type(v) == bool:
                    #     cursor.setinputsizes(**{k: cx_Oracle.BOOLEAN})
                    # if type(v) == bool:
                    #     b = cursor.var(cx_Oracle.BOOLEAN)
                    #     b.setvalue(0, v)
                    #     v = b
                    cursor_vars[k] = v

            cursor.execute(pl_sql_text, **cursor_vars)

            for k, v in kwargs.items():
                if v in oracle_types:
                    value = cursor_vars[k].getvalue()
                    if value and v in (cx_Oracle.CLOB, cx_Oracle.BLOB):
                        value = value.read()
                    cursor_vars[k] = value
                    out_vars[k] = value

        logger.debug(pretty_print(("pl/sql ответ за %s: " % (seconds_to_str(time.time() - begin_time))),
                                  pprint.pformat(out_vars, width=300)))
        return cursor_vars

    def select_gn(self, sql_text, row_num=None, **kwargs):
        """
        Делает селект и возвращает генератор со строками в виде словарей

        :param sql_text:
        :param kwargs:
        :return:
        """
        if kwargs.__len__() > 0:
            logger.debug(pretty_print("запрос: ", sql_text))
            logger.debug(pretty_print("параметры: ", pprint.pformat(kwargs)))
            for k, v in kwargs.items():
                kwargs[k] = numpy.asscalar(v) if isinstance(v, numpy.generic) else v
        else:
            logger.debug(pretty_print("запрос без параметров: ", sql_text))
        with contextlib.closing(self.connection.cursor()) as cursor:
            begin_time = time.time()
            cursor.execute(sql_text, **kwargs)
            desc = [d[0] for d in cursor.description]
            rows = []
            for i, row in enumerate(cursor):
                rows.append(row)
                if row_num and i >= row_num - 1:
                    break
            df = pd.DataFrame(rows, columns=desc)
            logger.debug(pretty_print(("ответ за %s: " % (seconds_to_str(time.time() - begin_time))), df))
            return df
            # for row in cursor:
            #     row_dict = dict(zip(desc, row))
            #     logger.debug("result for %s : %s" % (seconds_to_str(time.time() - begin_time), str(row_dict)))
            #     yield row_dict

        # rows = []
        # for i, row in enumerate(self.abs.select_gn(helpers.sql.rnd(sql_text, row_num=row_num), p_saldo=p_saldo)):
        #     rows.append(row)
        #     if i >= row_num - 1:
        #         break
        #
        # return rows

    # @log_call
    def select(self, sql_text, **kwargs):
        """
        Делает селект и возвращает pandas.DataFrame с данными

        :param sql_text:
        :param kwargs:
        :return:
        """
        begin_time = time.time()
        if kwargs.__len__() > 0:
            logger.debug(pretty_print("запрос: ", sql_text))
            logger.debug(pretty_print("параметры: ", pprint.pformat(kwargs)))
            for k, v in kwargs.items():
                kwargs[k] = numpy.asscalar(v) if isinstance(v, numpy.generic) else v
        else:
            logger.debug(pretty_print("запрос без параметров: ", sql_text))

        with contextlib.closing(self.connection.cursor()) as cursor:
            cursor.execute(sql_text, **kwargs)
            # cursor.arraysize = 5

            desc = []
            dtypes = {}
            for d in cursor.description:
                desc.append(d[0])
                if type(d[1]) == cx_Oracle.STRING:
                    dtypes[d[0]] = str

            df = pd.DataFrame(cursor.fetchall(), columns=desc)
            df = df.fillna('')

            str_df = ''
            if len(df.index) > 0:
                str_df = str(df)

            logger.debug(
                pretty_print("ответ за %s, вернул %s строк" % (seconds_to_str(time.time() - begin_time), len(df.index)),
                             str_df))
            return df

    cursors = {}

    def prepared_select(self, sql_text, **kwargs):
        """
        Делает селект и возвращает pandas.DataFrame с данными

        :param sql_text:
        :param kwargs:
        :return:
        """
        begin_time = time.time()
        if kwargs.__len__() > 0:
            logger.debug(pretty_print("запрос: ", sql_text))
            logger.debug(pretty_print("параметры: ", pprint.pformat(kwargs)))
        else:
            logger.debug(pretty_print("запрос без параметров: ", sql_text))

        cursor = self.cursors.get(sql_text)
        if cursor is None:
            cursor = self.cursors[sql_text] = self.connection.cursor()
            cursor.prepare(sql_text)
        cursor.execute(sql_text, **kwargs)
        # cursor.arraysize = 5
        desc = [d[0] for d in cursor.description]

        df = pd.DataFrame(cursor.fetchall(), columns=desc)
        df = df.fillna('')

        str_df = ''
        if len(df.index) > 0:
            str_df = str(df)

        logger.debug(
            pretty_print("ответ за %s, вернул %s строк" % (seconds_to_str(time.time() - begin_time), len(df.index)),
                         str_df))
        return df
