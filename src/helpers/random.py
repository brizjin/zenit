import random
import string
from datetime import date

from faker import Faker

faker = None

if not faker:
    faker = Faker("ru_RU")


def random_string(chars_count):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=chars_count))


def random_number(digits_count):
    return ''.join(random.choices(string.digits, k=digits_count))


# Дата рождения
def birdthay():
    random_day = date.fromordinal(random.randint(date.today().replace(year=1950).toordinal(),
                                                 date.today().replace(year=1990).toordinal())).strftime('%d.%m.%Y')
    return random_day


# Телефон
def phone_number():
    return faker.phone_number()


# Проверка на контрольную сумму
def ctrl_summ(nums, ntype):
    ctrl_type = {
        'n2_12': [7, 2, 4, 10, 3, 5, 9, 4, 6, 8],
        'n1_12': [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8],
        'n1_10': [2, 4, 10, 3, 5, 9, 4, 6, 8],
    }
    n = 0
    l = ctrl_type[ntype]
    for i in range(0, len(l)):
        n += nums[i] * l[i]
    return n % 11 % 10


# # СНИЛС
# def snils():
#     nums = [
#         random.randint(1, 1) if x == 0
#         else '-' if x == 3
#         else '-' if x == 7
#         else ' ' if x == 11
#         else random.randint(0, 9)
#         for x in range(0, 12)
#     ]
#
#     cont = (nums[10] * 1) + (nums[9] * 2) + (nums[8] * 3) + \
#            (nums[6] * 4) + (nums[5] * 5) + (nums[4] * 6) + \
#            (nums[2] * 7) + (nums[1] * 8) + (nums[0] * 9)
#
#     if cont in (100, 101):
#         cont = '00'
#
#     elif cont > 101:
#         cont = (cont % 101)
#         if cont in (100, 101):
#             cont = '00'
#         elif cont < 10:
#             cont = '0' + str(cont)
#
#     elif cont < 10:
#         cont = '0' + str(cont)
#
#     nums.append(cont)
#     return ''.join([str(x) for x in nums])


# Создание ИНН
def inn(l):
    nums = [
        random.randint(9, 9) if x == 0
        else random.randint(6, 6) if x == 1
        else random.randint(0, 9)
        for x in range(0, 9 if l == 10 else 10)
    ]

    if l == 10:
        n1 = ctrl_summ(nums, 'n1_10')
        nums.append(n1)

    elif l == 12:
        n2 = ctrl_summ(nums, 'n2_12')
        nums.append(n2)
        n1 = ctrl_summ(nums, 'n1_12')
        nums.append(n1)

    return ''.join([str(x) for x in nums])
