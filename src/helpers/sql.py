def first_rows(sql_text, row_num=1):
    return "select * from ({0}) where rownum <= {1}".format(sql_text, row_num)


def rnd(sql_text, row_num=None):
    sql_text = "select * from ({0}) order by dbms_random.value".format(sql_text)
    if row_num:
        # sql_text = "select * from ({0}) where rownum <= {1}".format(sql_text, row_num)
        sql_text = first_rows(sql_text, row_num)
    return sql_text


def count(sql_text):
    return "select count(1) from (%s)" % sql_text
