import lxml
import lxml.builder
import lxml.etree

e = lxml.builder.ElementMaker()


def tags(**kwargs):
    for key, value in kwargs.items():
        if type(value) == dict:
            yield e(key, *list(tags(**value)))
        elif type(value) == list:
            t = []
            for v in value:
                t.append(*list(tags(**v)))
            yield e(key, *t)
        else:
            yield e(key, str(value))


def to_stiring(xml):
    the_doc = xml
    if type(xml) == dict:
        for root_tag in tags(**xml):
            the_doc = root_tag
    return lxml.etree.tostring(the_doc, pretty_print=True, encoding="unicode")


def make_xml(root, **kwargs):
    the_doc = e(root, *[lxml.etree.XML(str(value)) if key.startswith('xml_') else e(key, str(value))
                        for key, value in kwargs.items()])
    return lxml.etree.tostring(the_doc, pretty_print=True, encoding="unicode")
