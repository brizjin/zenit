import calendar
import datetime
from functools import reduce


class Date(datetime.datetime):
    def add_months(self, months=1):
        if months == 0:
            return self
        month = self.month - 1 + months
        year = self.year + month // 12
        month = month % 12 + 1
        day = min(self.day, calendar.monthrange(year, month)[1])
        return Date(year, month, day, self.hour, self.minute, self.second, self.microsecond)


    def begin_month(self, months=0):
        return self.add_months(months).replace(day=1, hour=0, minute=0, second=0, microsecond=0)

    def end_month(self, months=0):
        second = datetime.timedelta(seconds=1)
        d = self.add_months(months).begin_month().add_months(1) - second
        return Date(d.year, d.month, d.day, d.hour, d.minute, d.second, d.microsecond)

    def begin_day(self):
        return self.replace(hour=0, minute=0, second=0, microsecond=0)

    def __str__(self):
        return self.strftime("%d.%m.%Y %H:%M:%S")

    def __repr__(self):
        return self.__str__()
    # def trim(self, interval='dd'):
    #     if interval == 'dd':
    #         return self.replace(hour=0, minute=0, second=0, microsecond=0)


def seconds_to_str(value):
    return "%d:%02d:%02d.%03d" % reduce(lambda ll, b: divmod(ll[0], b) + ll[1:], [(value * 1000,), 1000, 60, 60])
