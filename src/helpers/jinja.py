import datetime
import numbers

import jinja2
import pandas._libs.tslibs.timestamps

from helpers.date import Date


def inclause(array):
    return "(%s)" % ",".join(["'%s'" % a for a in array])


def sqlvar(obj):
    if type(obj) == str:
        return "'%s'" % obj
    if isinstance(obj, numbers.Number):
        return obj
    elif type(obj) == list:
        return "(%s)" % ",".join(["'%s'" % a for a in obj])
    elif type(obj) in (Date, datetime.datetime, pandas._libs.tslibs.timestamps.Timestamp):
        return "to_date('{value}','dd.mm.yyyy hh24:mi:ss')".format(value=obj.strftime("%d.%m.%Y %H:%M:%S"))
    elif obj is None:
        return "null"
    else:
        return str(obj)


env = jinja2.Environment()
env.filters['inclause'] = inclause
env.filters['sqlvar'] = sqlvar

env.line_statement_prefix = '%'
env.line_comment_prefix = '%%'


def template_render(text, **kwargs):
    return env.from_string(text).render(**kwargs)
