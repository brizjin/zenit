import logging
import logging.config
import logging.handlers
import os

PROJECT_ROOT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))



class FileHandler(logging.FileHandler):
    def __init__(self, filename, mode='a', encoding=None, delay=None):
        if not os.path.isabs(filename):
            filename = os.path.join(PROJECT_ROOT_DIR, filename)
        dirname = os.path.dirname(filename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        super().__init__(filename=filename, mode=mode, encoding=encoding, delay=delay)


class RotatingFileHandler(logging.handlers.RotatingFileHandler):
    def __init__(self, filename, mode='a', maxBytes=0, backupCount=0, encoding=None, delay=None):
        if not os.path.isabs(filename):
            filename = os.path.join(PROJECT_ROOT_DIR, filename)
        dirname = os.path.dirname(filename)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        super().__init__(filename=filename, maxBytes=maxBytes, backupCount=backupCount,
                         mode=mode, encoding=encoding, delay=delay)


def get_logger(file_name):
    logger_name = os.path.relpath(file_name, PROJECT_ROOT_DIR).replace("\\", '.')
    return logging.getLogger(logger_name)