import unittest

import config
from abs import Abs
import threading

from gen import Generators


class AbsTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.abs = Abs(config.DEFAULT_CONNECTION_STRING)
        cls.gen = Generators(cls.abs)

    def tearDown(self):
        self.abs.connection.rollback()

    def assertLen(self, expected_len, df):
        self.assertEqual(expected_len, len(df.index))


class TimeoutError(Exception):
    pass


class InterruptableThread(threading.Thread):
    def __init__(self, func, *args, **kwargs):
        threading.Thread.__init__(self)
        self._func = func
        self._args = args
        self._kwargs = kwargs
        self._result = None

    def run(self):
        self._result = self._func(*self._args, **self._kwargs)

    @property
    def result(self):
        return self._result


class timeout(object):
    def __init__(self, sec):
        self._sec = sec

    def __call__(self, f):
        def wrapped_f(*args, **kwargs):
            it = InterruptableThread(f, *args, **kwargs)
            it.start()
            it.join(self._sec)
            if not it.is_alive():
                return it.result
            raise TimeoutError('execution expired')

        return wrapped_f
