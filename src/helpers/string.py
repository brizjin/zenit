import textwrap

# from prettytable import PrettyTable
from veryprettytable import VeryPrettyTable


def is_id(value):
    try:
        return 10 ** 4 < int(value) < 10 ** 15
    except ValueError:
        return False


def lstripn_dedent(text):
    return textwrap.dedent(text.lstrip('\n'))


# def pretty(df, aligns=None):
#     x = PrettyTable()
#     x.field_names = df.columns
#     for i, row in df.iterrows():
#         x.add_row(row)
#
#     # if aligns:
#     #     for key, value in aligns.items():
#     #         x.align[key] = value
#     if aligns:
#         x.align.update(aligns)
#
#     return x.get_string()

# def pretty(df, aligns=None):
#     table = Texttable(max_width=1000)
#     table.add_row(df.columns)
#     for i, row in df.iterrows():
#         table.add_row(row)
#     return table.draw()

# def pretty(df, aligns=None):
#     table = BeautifulTable(max_width=1000)
#     table.column_headers = df.columns
#     for i, row in df.iterrows():
#         table.append_row(row)
#
#     # if aligns:
#     #     for key, value in aligns.items():
#     #         x.align[key] = value
#     # if aligns:
#     #     x.align.update(aligns)
#
#     return table.get_string(recalculate_width=False)

def pretty(df, aligns=None):
    x = VeryPrettyTable()
    x.field_names = df.columns
    for i, row in df.iterrows():
        x.add_row(row)

    # if aligns:
    #     for key, value in aligns.items():
    #         x.align[key] = value
    if aligns:
        x.align.update(aligns)

    return x.get_string()


def params_table(table_text, cut_header=True):
    text = lstripn_dedent(table_text).strip('\n')

    def convert(value):
        try:
            v = value.replace(',', '')
            return eval(v)
        except Exception:
            return value

    rows = [[convert(v.strip()) for v in row.split('|')] for row in text.split('\n')]
    if cut_header:
        return rows[1:]
    return rows


def params_table_from_file(file_name):
    with open(file_name, 'r+') as f:
        return params_table(f.read())
