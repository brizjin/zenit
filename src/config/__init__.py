import os
from pprint import pprint

import yaml
import logging.config

import pandas as pd


TESTRAIL = None
SELECT_HISTORY = {"USE": False, "FOLDER": None}
SELECT_CACHE = {"USE": False, "FOLDER": None}


config_file_name = os.path.join(os.path.dirname(__file__), "config.yml")
self = globals()
with open(config_file_name, "r") as f:
    config_dict = yaml.load(f.read())
    self.update(config_dict)

DEFAULT_CONNECTION_STRING = self["TNS"][self["DEFAULT_TNS"]]

PROJECT_ROOT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.path.realpath(__file__), '../..')))
TESTS_ROOT_DIR = os.path.join(PROJECT_ROOT_DIR, 'tests')

if not self.get("CSV_FOLDER"):
    CSV_FOLDER = os.path.join(PROJECT_ROOT_DIR, 'csv')
if not os.path.exists(CSV_FOLDER):
    os.makedirs(CSV_FOLDER)


if SELECT_CACHE["USE"] and SELECT_CACHE["FOLDER"]:
    if not os.path.isabs(SELECT_CACHE["FOLDER"]):
        SELECT_CACHE["FOLDER"] = os.path.join(PROJECT_ROOT_DIR, SELECT_CACHE["FOLDER"])
    if not os.path.exists(SELECT_CACHE["FOLDER"]):
        os.makedirs(SELECT_CACHE["FOLDER"])

if SELECT_HISTORY["USE"] and SELECT_HISTORY["FOLDER"]:
    if not os.path.isabs(SELECT_HISTORY["FOLDER"]):
        SELECT_HISTORY["FOLDER"] = os.path.join(PROJECT_ROOT_DIR, SELECT_HISTORY["FOLDER"])
    if not os.path.exists(SELECT_HISTORY["FOLDER"]):
        os.makedirs(SELECT_HISTORY["FOLDER"])


pd.options.display.width = 2000
pd.set_option('display.max_columns', None)

# def date_representer(dumper, data):
#     # return dumper.represent_scalar(u'!Date', u'(%s)' % data.strftime("%d.%m.%Y %H:%M:%S"))
#     return dumper.represent_scalar(u'tag:yaml.org,2002:str', u'Date(%s)' % str(data))
#
#
# yaml.add_representer(Date, date_representer)

# logging.getLogger('oracle_client').setLevel(self["ORACLE_CLIENT_DEBUG_LEVEL"])


logging_config = self.get("LOGGING")
if logging_config:
    logging.config.dictConfig(logging_config)

# pprint(os.path)